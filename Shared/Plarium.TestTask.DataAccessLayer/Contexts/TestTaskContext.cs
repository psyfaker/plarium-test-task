﻿using Plarium.TestTask.DataAccessLayer.Entities;
using System.Data.Entity;

namespace Plarium.TestTask.DataAccessLayer.Contexts
{
    /// <summary>
    /// Test task entity framework context
    /// </summary>
    /// <seealso cref="System.Data.Entity.DbContext" />
    public class TestTaskContext : DbContext
    {
        /// <summary>
        /// Gets or sets the log models.
        /// </summary>
        /// <value>
        /// The log models.
        /// </value>
        public DbSet<LogModel> LogModels { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TestTaskContext"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public TestTaskContext(string connectionString)
            : base(connectionString)
        {
        }
    }
}