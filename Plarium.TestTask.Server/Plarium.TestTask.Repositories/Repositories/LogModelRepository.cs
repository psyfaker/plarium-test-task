﻿using Plarium.TestTask.DataAccessLayer.Contexts;
using Plarium.TestTask.DataAccessLayer.Entities;
using Plarium.TestTask.Server.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Plarium.TestTask.Server.Repositories.Repositories
{
    /// <summary>
    /// Log model repository
    /// </summary>
    /// <seealso cref="Plarium.TestTask.Server.Repositories.Interfaces.ILogModelRepository" />
    public class LogModelRepository : ILogModelRepository
    {
        /// <summary>
        /// The context
        /// </summary>
        private readonly TestTaskContext _context;

        /// <summary>
        /// Initializes a new instance of the <see cref="LogModelRepository"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public LogModelRepository(TestTaskContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Gets fixed count of the most popular hosts.
        /// </summary>
        /// <param name="count">The count.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public async Task<List<string>> GetHosts(int count, DateTime? startDate, DateTime? endDate)
        {
            var filter = CreateDateFilter(startDate, endDate);

            var hosts = await _context.LogModels
                .Where(filter)
                .GroupBy(l => l.Host, l => l.Id)
                .OrderByDescending(g => g.Count())
                .Select(g => g.Key)
                .Take(count)
                .ToListAsync();

            return hosts;
        }

        /// <summary>
        /// Gets fixed count of the most popular routes.
        /// </summary>
        /// <param name="count">The count.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns></returns>
        public async Task<List<string>> GetRoutes(int count, DateTime? startDate, DateTime? endDate)
        {
            var filter = CreateDateFilter(startDate, endDate);

            var hosts = await _context.LogModels
                .Where(filter)
                .GroupBy(l => l.Route, l => l.Id)
                .OrderByDescending(g => g.Count())
                .Select(g => g.Key)
                .Take(count)
                .ToListAsync();

            return hosts;
        }

        /// <summary>
        /// Gets the logs sorted by encreasing time
        /// </summary>
        /// <param name="offset">The offset.</param>
        /// <param name="count">The count.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public async Task<List<LogModel>> GetLogs(int? offset, int count, DateTime? startDate, DateTime? endDate)
        {
            var filter = CreateLogFilter(startDate, endDate, offset);

            var logs = await _context.LogModels
                .OrderBy(l => l.RequestTime)
                .Where(filter)
                .Take(count)
                .ToListAsync();

            return logs;
        }

        /// <summary>
        /// Creates the log filter.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="offset">The offset.</param>
        /// <returns></returns>
        private Expression<Func<LogModel, bool>> CreateLogFilter(DateTime? startDate, DateTime? endDate, int? offset)
        {
            var requestTimeFilter = CreateDateFilter(startDate, endDate);
            if (offset != null)
            {
                var logModelParam = Expression.Parameter(typeof(LogModel), "log");
                var idProperty = Expression.Property(logModelParam, "Id");

                var offsetValue = Expression.Constant(offset);

                var offsetExpression = Expression.GreaterThanOrEqual(idProperty, offsetValue);
                var combineExpressions = Expression.AndAlso(requestTimeFilter.Body, offsetExpression);

                return Expression.Lambda<Func<LogModel, bool>>(combineExpressions, logModelParam);              
            }

            return requestTimeFilter;
        }

        /// <summary>
        /// Creates the filter.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns></returns>
        private Expression<Func<LogModel, bool>> CreateDateFilter(DateTime? startDate, DateTime? endDate)
        {
            var logModelParam = Expression.Parameter(typeof(LogModel), "log");
            var requestTimeProperty = Expression.Property(logModelParam, "RequestTime");

            var requestTimeFilter = Expression.Lambda<Func<LogModel, bool>>(Expression.Constant(true), logModelParam);

            if (startDate != null || endDate != null)
            {
                if (startDate != null)
                {
                    var startDateValue = Expression.Constant(startDate);
                    var startDateExpression = Expression.GreaterThanOrEqual(requestTimeProperty, startDateValue);
                    var combineExpressions = Expression.AndAlso(requestTimeFilter.Body, startDateExpression);

                    requestTimeFilter = Expression.Lambda<Func<LogModel, bool>>(combineExpressions, logModelParam);
                }

                if (endDate != null)
                {
                    var endDateValue = Expression.Constant(endDate);
                    var endDateExpression = Expression.LessThanOrEqual(requestTimeProperty, endDateValue);
                    var combineExpressions = Expression.AndAlso(requestTimeFilter.Body, endDateExpression);

                    requestTimeFilter = Expression.Lambda<Func<LogModel, bool>>(combineExpressions, logModelParam);
                }
            }

            return requestTimeFilter;
        }
    }
}