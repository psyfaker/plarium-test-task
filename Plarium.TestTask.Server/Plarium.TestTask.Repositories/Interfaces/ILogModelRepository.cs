﻿using Plarium.TestTask.DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Plarium.TestTask.Server.Repositories.Interfaces
{
    public interface ILogModelRepository
    {
        /// <summary>
        /// Gets fixed count of logs.
        /// </summary>
        /// <param name="count">The count.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns></returns>
        Task<List<string>> GetHosts(int count, DateTime? startDate, DateTime? endDate);

        /// <summary>
        /// Gets fixed count of routes.
        /// </summary>
        /// <param name="count">The count.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns></returns>
        Task<List<string>> GetRoutes(int count, DateTime? startDate, DateTime? endDate);

        /// <summary>
        /// Gets the logs sorted by encreasing time
        /// </summary>
        /// <param name="offset">The offset.</param>
        /// <param name="count">The count.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns></returns>
        Task<List<LogModel>> GetLogs(int? offset, int count, DateTime? startDate, DateTime? endDate);
    }
}