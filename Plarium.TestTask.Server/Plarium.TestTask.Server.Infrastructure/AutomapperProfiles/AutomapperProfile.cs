﻿using AutoMapper;
using Plarium.TestTask.DataAccessLayer.Entities;
using Plarium.TestTask.Server.DataTransferObjects;

namespace Plarium.TestTask.Server.Infrastructure.AutomapperProfiles
{
    /// <summary>
    /// Automapper profile
    /// </summary>
    /// <seealso cref="AutoMapper.Profile" />
    public class AutoMapperProfile : Profile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AutoMapperProfile"/> class.
        /// </summary>
        public AutoMapperProfile()
        {
            CreateMap<LogModel, LogDto>();
        }
    }
}