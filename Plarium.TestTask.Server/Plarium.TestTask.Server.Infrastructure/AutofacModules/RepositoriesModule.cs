﻿using System.Reflection;
using Autofac;
using Plarium.TestTask.DataAccessLayer.Contexts;
using Module = Autofac.Module;

namespace Plarium.TestTask.Server.Infrastructure.AutofacModules
{
    /// <summary>
    /// Autofac repisotires dependencies setup module
    /// </summary>
    /// <seealso cref="Autofac.Module" />
    public class RepositoriesModule : Module
    {
        /// <summary>
        /// The connection string
        /// </summary>
        private readonly string _connectionString;

        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoriesModule"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public RepositoriesModule(string connectionString)
        {
            _connectionString = connectionString;
        }

        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <param name="builder">The builder through which components can be
        /// registered.</param>
        /// <remarks>
        /// Note that the ContainerBuilder parameter is unique to this module.
        /// </remarks>
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(Assembly.Load("Plarium.TestTask.Server.Repositories"))
                .Where(t => t.Name.EndsWith("Repository"))
                .AsImplementedInterfaces()
                .WithParameter(new NamedParameter("context", new TestTaskContext(_connectionString)))
                .InstancePerLifetimeScope();
        }
    }
}