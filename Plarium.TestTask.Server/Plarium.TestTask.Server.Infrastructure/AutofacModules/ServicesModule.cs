﻿using System.Reflection;
using Autofac;
using AutoMapper;
using Plarium.TestTask.Server.Infrastructure.AutomapperProfiles;
using Module = Autofac.Module;

namespace Plarium.TestTask.Server.Infrastructure.AutofacModules
{
    /// <summary>
    /// Autofac services dependencies setup module
    /// </summary>
    /// <seealso cref="Autofac.Module" />
    public class ServicesModule : Module
    {
        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <param name="builder">The builder through which components can be
        /// registered.</param>
        /// <remarks>
        /// Note that the ContainerBuilder parameter is unique to this module.
        /// </remarks>
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(Assembly.Load("Plarium.TestTask.Server.Services"))
                .Where(t => t.Name.EndsWith("Service"))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            //register configuration as a single instance
            builder.Register(c => new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AutoMapperProfile>();
            })).AsSelf().SingleInstance();

            //register mapper
            builder.Register(c => c.Resolve<MapperConfiguration>().CreateMapper(c.Resolve)).As<IMapper>().InstancePerLifetimeScope();
        }
    }
}