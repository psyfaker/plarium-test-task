﻿using System;

namespace Plarium.TestTask.Server.DataTransferObjects.RequestDtos
{
    /// <summary>
    /// 
    /// </summary>
    public class HostRequestDto
    {
        /// <summary>
        /// Gets or sets the n.
        /// </summary>
        /// <value>
        /// The n.
        /// </value>
        public int N { get; set; } = 10;

        /// <summary>
        /// Gets or sets the start.
        /// </summary>
        /// <value>
        /// The start.
        /// </value>
        public DateTime? Start { get; set; }

        /// <summary>
        /// Gets or sets the end.
        /// </summary>
        /// <value>
        /// The end.
        /// </value>
        public DateTime? End { get; set; }
    }
}