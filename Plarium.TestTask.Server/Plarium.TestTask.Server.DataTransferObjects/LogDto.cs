﻿using System;

namespace Plarium.TestTask.Server.DataTransferObjects
{
    /// <summary>
    /// Log data transfer object
    /// </summary>
    public class LogDto
    {
        /// <summary>
        /// Gets or sets the host.
        /// </summary>
        /// <value>
        /// The host.
        /// </value>
        public string Host { get; set; }

        /// <summary>
        /// Gets or sets the request time.
        /// </summary>
        /// <value>
        /// The request time.
        /// </value>
        public DateTime RequestTime { get; set; }

        /// <summary>
        /// Gets or sets the route.
        /// </summary>
        /// <value>
        /// The route.
        /// </value>
        public string Route { get; set; }

        /// <summary>
        /// Gets or sets the URL query parameters.
        /// </summary>
        /// <value>
        /// The URL query parameters.
        /// </value>
        public string UrlQueryParameters { get; set; }

        /// <summary>
        /// Gets or sets the result.
        /// </summary>
        /// <value>
        /// The result.
        /// </value>
        public int Result { get; set; }

        /// <summary>
        /// Gets or sets the size.
        /// </summary>
        /// <value>
        /// The size.
        /// </value>
        public double Size { get; set; }

        /// <summary>
        /// Gets or sets country name.
        /// </summary>
        /// <value>
        /// The country name
        /// </value>
        public string CountryName { get; set; }
    }
}