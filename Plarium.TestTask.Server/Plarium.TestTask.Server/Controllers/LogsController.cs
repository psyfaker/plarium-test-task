﻿using System.Threading.Tasks;
using Plarium.TestTask.Server.DataTransferObjects.RequestDtos;
using Plarium.TestTask.Server.Services.Interfaces;
using System.Web.Http;

namespace Plarium.TestTask.Server.Controllers
{
    /// <summary>
    /// Default app controller
    /// </summary>
    /// <seealso cref="System.Web.Mvc.Controller" />
    public class LogsController : ApiController
    {
        /// <summary>
        /// The logs service
        /// </summary>
        private readonly ILogsService _logsService;

        /// <summary>
        /// Initializes a new instance of the <see cref="LogsController"/> class.
        /// </summary>
        /// <param name="logsService">The logs service.</param>
        public LogsController(ILogsService logsService)
        {
            _logsService = logsService;
        }

        /// <summary>
        /// Gets the hosts.
        /// </summary>
        /// <param name="hostRequestDto">The host request dto.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Logs/GetHosts")]
        public async Task<IHttpActionResult> GetHosts(HostRequestDto hostRequestDto)
        {
            var hosts = await _logsService.GetTopHosts(hostRequestDto);
            if (hosts == null)
                return BadRequest();

            return Ok(hosts);
        }

        /// <summary>
        /// Gets the routes.
        /// </summary>
        /// <param name="routesRequestDto">The routes request dto.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Logs/GetTopRoutes")]
        public async Task<IHttpActionResult> GetRoutes(RoutesRequestDto routesRequestDto)
        {
            var routes = await _logsService.GetTopRoutes(routesRequestDto);
            if (routes == null)
                return BadRequest();

            return Ok(routes);
        }

        /// <summary>
        /// Gets the logs.
        /// </summary>
        /// <param name="logsRequestDto">The logs request dto.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Logs/GetLogs")]
        public async Task<IHttpActionResult> GetLogs(LogsRequestDto logsRequestDto)
        {
            var logs = await _logsService.GetLogs(logsRequestDto);
            if (logs == null)
                return BadRequest();

            return Ok(logs);
        }
    }
}