﻿using System.Configuration;
using Autofac;
using Autofac.Integration.WebApi;
using Plarium.TestTask.Server.Infrastructure.AutofacModules;
using System.Reflection;
using System.Web.Http;

namespace Plarium.TestTask.Server
{
    /// <summary>
    ///  Autofac Webapi config
    /// </summary>
    public class AutofacWebapiConfig
    {
        /// <summary>
        /// The container
        /// </summary>
        public static IContainer Container;

        /// <summary>
        /// Initializes the specified configuration.
        /// </summary>
        /// <param name="config">The configuration.</param>
        public static void Initialize(HttpConfiguration config)
        {
            Initialize(config, RegisterServices(new ContainerBuilder()));
        }


        /// <summary>
        /// Initializes the specified configuration.
        /// </summary>
        /// <param name="config">The configuration.</param>
        /// <param name="container">The container.</param>
        public static void Initialize(HttpConfiguration config, IContainer container)
        {
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }

        /// <summary>
        /// Registers the services.
        /// </summary>
        /// <param name="builder">The builder.</param>
        /// <returns></returns>
        private static IContainer RegisterServices(ContainerBuilder builder)
        {
            //Register Web API controllers.  
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            builder.RegisterModule(new ServicesModule());

            var connectionString = ConfigurationManager.AppSettings["ConnectionString"];
            builder.RegisterModule(new RepositoriesModule(connectionString));

            //Set the dependency resolver to be Autofac.  
            Container = builder.Build();

            return Container;
        }
    }
}