using System.Web.Http;
using WebActivatorEx;
using Plarium.TestTask.Server;
using Swashbuckle.Application;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace Plarium.TestTask.Server
{
    /// <summary>
    /// Swagger Config
    /// </summary>
    public class SwaggerConfig
    {
        /// <summary>
        /// Registers this instance.
        /// </summary>
        public static void Register()
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;

            GlobalConfiguration.Configuration
                .EnableSwagger(c =>
                {
                    c.SingleApiVersion("v1", "Plarium.TestTask.Server");
                    c.IncludeXmlComments(string.Format(@"{0}\bin\Plarium.TestTask.Server.xml",
                        System.AppDomain.CurrentDomain.BaseDirectory));
                })
                .EnableSwaggerUi();
        }
    }
}