﻿using Plarium.TestTask.Server.DataTransferObjects;
using Plarium.TestTask.Server.DataTransferObjects.RequestDtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Plarium.TestTask.Server.Services.Interfaces
{
    /// <summary>
    /// Logs service interface
    /// </summary>
    public interface ILogsService
    {
        /// <summary>
        /// Gets the top hosts sorted by decreasing queries count
        /// </summary>
        /// <param name="hostRequestDto">The host request dto.</param>
        /// <returns></returns>
        Task<List<string>> GetTopHosts(HostRequestDto hostRequestDto);

        /// <summary>
        /// Gets the routes.
        /// </summary>
        /// <param name="routesRequestDto">The routes request dto.</param>
        /// <returns></returns>
        Task<List<string>> GetTopRoutes(RoutesRequestDto routesRequestDto);

        /// <summary>
        /// Gets the logs sorted by encreasing time
        /// </summary>
        /// <param name="logsRequestDto">The logs request dto.</param>
        /// <returns></returns>
        Task<List<LogDto>> GetLogs(LogsRequestDto logsRequestDto);
    }
}