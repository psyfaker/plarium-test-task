﻿using AutoMapper;
using Plarium.TestTask.DataAccessLayer.Entities;
using Plarium.TestTask.Server.DataTransferObjects;
using Plarium.TestTask.Server.DataTransferObjects.RequestDtos;
using Plarium.TestTask.Server.Repositories.Interfaces;
using Plarium.TestTask.Server.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Plarium.TestTask.Server.Services.Services
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Plarium.TestTask.Server.Services.Interfaces.ILogsService" />
    public class LogsService : ILogsService
    {
        /// <summary>
        /// The log model repository
        /// </summary>
        private readonly ILogModelRepository _logModelRepository;

        /// <summary>
        /// The mapper
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="LogsService" /> class.
        /// </summary>
        /// <param name="logModelRepository">The log model repository.</param>
        /// <param name="mapper">The mapper.</param>
        public LogsService(ILogModelRepository logModelRepository, IMapper mapper)
        {
            _logModelRepository = logModelRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets the top hosts sorted by decreasing queries count
        /// </summary>
        /// <param name="hostRequestDto">The host request dto.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public async Task<List<string>> GetTopHosts(HostRequestDto hostRequestDto)
        {
            var hosts = await _logModelRepository.GetHosts(hostRequestDto.N, hostRequestDto.Start, hostRequestDto.End);

            return hosts;
        }

        /// <summary>
        /// Gets the routes.
        /// </summary>
        /// <param name="routesRequestDto">The routes request dto.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public async Task<List<string>> GetTopRoutes(RoutesRequestDto routesRequestDto)
        {
            var routes = await _logModelRepository.GetRoutes(routesRequestDto.N, routesRequestDto.Start, routesRequestDto.End);

            return routes;
        }

        /// <summary>
        /// Gets the logs sorted by encreasing time
        /// </summary>
        /// <param name="logsRequestDto">The logs request dto.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public async Task<List<LogDto>> GetLogs(LogsRequestDto logsRequestDto)
        {
            var logModelList = await _logModelRepository.GetLogs(logsRequestDto.Offset, logsRequestDto.Limit,
                logsRequestDto.Start, logsRequestDto.End);

            var logDtoList = _mapper.Map<List<LogModel>, List<LogDto>>(logModelList);

            return logDtoList;
        }
    }
}