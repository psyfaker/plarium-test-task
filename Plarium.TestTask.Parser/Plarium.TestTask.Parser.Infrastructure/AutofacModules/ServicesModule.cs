﻿using Autofac;
using System.Reflection;
using Module = Autofac.Module;

namespace Plarium.TestTask.Parser.Infrastructure.AutofacModules
{
    /// <summary>
    /// Autofac services dependencies setup module
    /// </summary>
    /// <seealso cref="Autofac.Module" />
    public class ServicesModule : Module
    {
        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <param name="builder">The builder through which components can be
        /// registered.</param>
        /// <remarks>
        /// Note that the ContainerBuilder parameter is unique to this module.
        /// </remarks>
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(Assembly.Load("Plarium.TestTask.Parser.Services"))
                .Where(t => t.Name.EndsWith("Service"))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
        }
    }
}