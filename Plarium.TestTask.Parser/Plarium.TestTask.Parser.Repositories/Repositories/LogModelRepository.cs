﻿using System.Threading.Tasks;
using Plarium.TestTask.DataAccessLayer.Contexts;
using Plarium.TestTask.DataAccessLayer.Entities;
using Plarium.TestTask.Parser.Repositories.Interfaces;

namespace Plarium.TestTask.Parser.Repositories.Repositories
{
    /// <summary>
    /// Log model repository
    /// </summary>
    /// <seealso cref="Plarium.TestTask.Parser.Repositories.Interfaces.ILogModelRepository" />
    public class LogModelRepository : ILogModelRepository
    {
        /// <summary>
        /// The context
        /// </summary>
        private readonly TestTaskContext _context;

        /// <summary>
        /// Initializes a new instance of the <see cref="LogModelRepository"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public LogModelRepository(TestTaskContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Adds the specified log model.
        /// </summary>
        /// <param name="logModel">The log model.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public void Add(LogModel logModel)
        {
            _context.LogModels.Add(logModel);
        }

        /// <summary>
        /// Saves the changes.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public async Task SaveChanges()
        {
            await _context.SaveChangesAsync();
        }
    }
}