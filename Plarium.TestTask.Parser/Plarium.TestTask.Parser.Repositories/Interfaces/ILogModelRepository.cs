﻿using System.Threading.Tasks;
using Plarium.TestTask.DataAccessLayer.Entities;

namespace Plarium.TestTask.Parser.Repositories.Interfaces
{
    /// <summary>
    /// Log model repository's interface
    /// </summary>
    public interface ILogModelRepository
    {
        /// <summary>
        /// Adds the specified log model.
        /// </summary>
        /// <param name="logModel">The log model.</param>
        /// <returns></returns>
        void Add(LogModel logModel);

        /// <summary>
        /// Saves the changes.
        /// </summary>
        /// <returns></returns>
        Task SaveChanges();
    }
}