﻿using Autofac;
using Plarium.TestTask.Parser.Services.Interfaces;
using System;
using System.Configuration;
using System.Windows.Forms;

namespace Plarium.TestTask.Parser
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            var connectionString = ConfigurationManager.AppSettings["ConnectionString"];

            // Begin autofac container lite time scope
            var container = ContainerConfig.Configure(connectionString);
            using (var scope = container.BeginLifetimeScope())
            {
                var logsParsingService = scope.Resolve<ILogsParsingService>();

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Form1(logsParsingService));
            }
        }
    }
}