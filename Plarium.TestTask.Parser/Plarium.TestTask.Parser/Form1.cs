﻿using Plarium.TestTask.Parser.Services.Interfaces;
using System;
using System.Windows.Forms;

namespace Plarium.TestTask.Parser
{
    /// <summary>
    /// Main application form class 
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class Form1 : Form
    {
        /// <summary>
        /// The logs parsing service
        /// </summary>
        private readonly ILogsParsingService _logsParsingService;

        /// <summary>
        /// Initializes a new instance of the <see cref="Form1"/> class.
        /// </summary>
        public Form1(ILogsParsingService logsParsingService)
        {
           _logsParsingService = logsParsingService;
            InitializeComponent();
        }

        /// <summary>
        /// Handles the Click event of the button1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private async void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() != DialogResult.OK)
                return;

            var isSuccessfullyParsed = await _logsParsingService.ParseLogFile(openFileDialog1.FileName);

            if (isSuccessfullyParsed)
            {
                MessageBox.Show("Log file is successfully parsed and saved");
                return;
            }

            MessageBox.Show("Sorry, try again");
        }
    }
}