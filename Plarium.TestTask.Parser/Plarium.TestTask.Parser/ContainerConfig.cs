﻿using Autofac;
using Plarium.TestTask.Parser.Infrastructure.AutofacModules;

namespace Plarium.TestTask.Parser
{
    /// <summary>
    /// Autofac container config
    /// </summary>
    public static class ContainerConfig
    {
        /// <summary>
        /// Configures this instance.
        /// </summary>
        /// <returns></returns>
        public static IContainer Configure(string connectionString)
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule(new ServicesModule());
            builder.RegisterModule(new RepositoriesModule(connectionString));

            return builder.Build();
        }
    }
}