﻿using Plarium.TestTask.DataAccessLayer.Entities;
using Plarium.TestTask.Parser.Repositories.Interfaces;
using Plarium.TestTask.Parser.Services.Interfaces;
using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Plarium.TestTask.Parser.Services.Services
{
    /// <summary>
    /// Logs parsing service
    /// </summary>
    public class LogsParsingService : ILogsParsingService
    {
        /// <summary>
        /// The log model repository
        /// </summary>
        private readonly ILogModelRepository _logModelRepository;

        /// <summary>
        /// The geo location service
        /// </summary>
        private readonly IGeoLocationService _geoLocationService;

        /// <summary>
        /// Initializes a new instance of the <see cref="LogsParsingService" /> class.
        /// </summary>
        /// <param name="logModelRepository">The log model repository.</param>
        /// <param name="geoLocationService">The geo location service.</param>
        public LogsParsingService(ILogModelRepository logModelRepository, IGeoLocationService geoLocationService)
        {
            _logModelRepository = logModelRepository;
            _geoLocationService = geoLocationService;
        }

        /// <summary>
        /// Parses the log file.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public async Task<bool> ParseLogFile(string filePath)
        {
            string[] logFileLines = File.ReadAllLines(filePath);

            // Regex patterns defenition
            var ipAdressPattern = new Regex(@"(?<ip_adress>\S+)");
            var timestampPattern = new Regex(@"(?<timestamp>\d{2}/\w{3}/\d{4}:\d{2}:\d{2}:\d{2} -\d{4})");
            var routePattern = new Regex(@"/(?<route>\S+\D+) ");
            var resultStatusCodePattern = new Regex(@" (?<result>\d{3}) ");
            var requestResultSizePattern = new Regex(@"\d{3} (?<size>)\d+");
            var urlQueryParametersPattern = new Regex(@"\?(?<parameters>)\S+");

            var redundantExtensions = new[] { "gif", "jpg", "png","jpeg", "css", "js", "htm" };

            foreach (var logFileLine in logFileLines)
            {
                var requestRoute = routePattern.Match(logFileLine).Value;

                //Checking, if request is not for taking image or css style or script
                if (redundantExtensions.All(re => requestRoute.IndexOf(re, StringComparison.OrdinalIgnoreCase) == -1))
                {
                    var logModel = new LogModel()
                    {
                        Host = ipAdressPattern.Match(logFileLine).Value,
                        Route = requestRoute,
                        RequestTime = DateTime.ParseExact(timestampPattern.Match(logFileLine).Value,
                            "dd/MMM/yyyy:HH:mm:ss zzz",
                            System.Globalization.CultureInfo.InvariantCulture),
                        Result = Convert.ToInt32(resultStatusCodePattern.Match(logFileLine).Value.Replace(" ", "")),                      
                    };

                    //logModel.CountryName = await _geoLocationService.GetContryByHost(logModel.Host);

                    // Checking route for additional url query parameters
                    var urlQueryParametersMatch = urlQueryParametersPattern.Match(logFileLine);
                    if (urlQueryParametersMatch.Success)
                    {
                        logModel.UrlQueryParameters = urlQueryParametersMatch.Value;
                        // Remove url query parametrs from route field
                        logModel.Route = logModel.Route.Split('?')[0];
                    }

                    // If request is not successful that result size is undefined
                    var requestResultSizeMatch = requestResultSizePattern.Match(logFileLine);
                    if (requestResultSizeMatch.Success)
                    {
                        logModel.Size = Convert.ToInt32(requestResultSizeMatch.Value.Split(' ')[1]);
                    }

                    _logModelRepository.Add(logModel);
                }
            }

            try
            {
                await _logModelRepository.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}