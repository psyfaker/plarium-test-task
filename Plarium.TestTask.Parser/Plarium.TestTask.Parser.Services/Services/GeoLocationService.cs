﻿using Newtonsoft.Json;
using Plarium.TestTask.Parser.Services.Interfaces;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Plarium.TestTask.Parser.Services.Services
{
    public class GeoLocationService : IGeoLocationService
    {
        /// <summary>
        /// The API URI
        /// </summary>
        private readonly string _apiUri;

        /// <summary>
        /// Initializes a new instance of the <see cref="GeoLocationService"/> class.
        /// </summary>
        public GeoLocationService()
        {
            _apiUri = "http://freegeoip.net/json/";
        }

        /// <summary>
        /// Gets the contry by host.
        /// </summary>
        /// <param name="host">The host.</param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<string> GetContryByHost(string host)
        {
            var client = new HttpClient();

            try
            {
                var response = await client.GetAsync(new Uri(_apiUri + host));
                if (response.StatusCode != HttpStatusCode.OK)
                    return "";

                var content = await response.Content.ReadAsStringAsync();

                dynamic test = JsonConvert.DeserializeObject(content);
                return test.country_name.ToString();
            }
            catch (Exception)
            {
                return "";
            }
        }
    }
}
