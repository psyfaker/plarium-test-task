﻿using System.Threading.Tasks;

namespace Plarium.TestTask.Parser.Services.Interfaces
{
    /// <summary>
    /// Logs parsing service's interface
    /// </summary>
    public interface ILogsParsingService
    {
        /// <summary>
        /// Parses the log file.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <returns></returns>
        Task<bool> ParseLogFile(string filePath);
    }
}