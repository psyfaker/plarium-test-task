﻿using System.Threading.Tasks;

namespace Plarium.TestTask.Parser.Services.Interfaces
{
    /// <summary>
    /// Geo location service's interface
    /// </summary>
    public interface IGeoLocationService
    {
        /// <summary>
        /// Gets the contry by host.
        /// </summary>
        /// <param name="host">The host.</param>
        /// <returns></returns>
        Task<string> GetContryByHost(string host);
    }
}